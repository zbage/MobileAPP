/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50704
Source Host           : localhost:3306
Source Database       : 36kr

Target Server Type    : MYSQL
Target Server Version : 50704
File Encoding         : 65001

Date: 2015-09-28 16:42:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `artical_detail`
-- ----------------------------
DROP TABLE IF EXISTS `artical_detail`;
CREATE TABLE `artical_detail` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(10) NOT NULL COMMENT '文章标题',
  `content` text COMMENT '文章内容',
  `author` varchar(255) DEFAULT '无名氏' COMMENT '文章作者',
  `pdate` date DEFAULT NULL COMMENT '发布时间',
  `comment` text COMMENT '评论',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of artical_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `article_outline`
-- ----------------------------
DROP TABLE IF EXISTS `article_outline`;
CREATE TABLE `article_outline` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(10) NOT NULL COMMENT '标题',
  `thumb` varchar(80) NOT NULL DEFAULT 'degault.jpg' COMMENT '文章缩略图',
  `introduce` varchar(255) NOT NULL DEFAULT '文章简介',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article_outline
-- ----------------------------
